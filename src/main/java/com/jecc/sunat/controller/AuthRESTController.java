package com.jecc.sunat.controller;


import com.jecc.sunat.client.AuthClient;
import com.jecc.sunat.client.LoginRepository;
import com.jecc.sunat.model.AuthRequest;
import com.jecc.sunat.model.AuthResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("auth")
public class AuthRESTController {
    private final Logger log = LoggerFactory.getLogger(AuthRESTController.class);


    private final AuthClient authClient;
    private final LoginRepository loginRepository;

    @Autowired
    public AuthRESTController(AuthClient authClient, LoginRepository loginRepository) {
        this.authClient = authClient;
        this.loginRepository = loginRepository;
    }

    @GetMapping
    public ResponseEntity<AuthResponse> getToken() {

        AuthRequest
                authRequest = new AuthRequest(
                "password",
                "c53c1d64-39ea-45b4-ae9b-82f7cca3d947",
                "HlAhwkfhP1uBAnkkFmcPcg==",
                "20563358549" + "E7215572",
                "Redsol1003",
                "https://api-cpe.sunat.gob.pe/"
        );

        Map<String, Object> requestParams = new HashMap<>();
        requestParams.put("grant_type", "password");
        requestParams.put("client_id", "c53c1d64-39ea-45b4-ae9b-82f7cca3d947");
        requestParams.put("client_secret", "HlAhwkfhP1uBAnkkFmcPcg==");
        requestParams.put("username", "20563358549" + "E7215572");
        requestParams.put("password", "Redsol1003");
        requestParams.put("scope", "https://api-cpe.sunat.gob.pe/");

        AuthResponse response = loginRepository.signIn(requestParams);
        return ResponseEntity.ok(response);


//        AuthResponse authResponse = authClient.getToken("password", "c53c1d64-39ea-45b4-ae9b-82f7cca3d947",
//                "HlAhwkfhP1uBAnkkFmcPcg==", "20563358549" + "E7215572", "Redsol1003", "https://api-cpe.sunat.gob.pe/");
//
//        WebClient webClient = WebClient.builder()
//                .baseUrl("https://api-seguridad.sunat.gob.pe/v1/clientessol/c53c1d64-39ea-45b4-ae9b-82f7cca3d947/oauth2/token")
//                .build();
//
//        AuthResponse block = webClient.post()
//                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
//                .body(BodyInserters.fromFormData("grant_type", "password")
//                        .with("client_id", "c53c1d64-39ea-45b4-ae9b-82f7cca3d947")
//                        .with("client_secret", "HlAhwkfhP1uBAnkkFmcPcg==")
//                        .with("username", "20563358549" + "E7215572")
//                        .with("password", "Redsol1003")
//                        .with("scope", "https://api-cpe.sunat.gob.pe/")
//                )
//                .exchangeToMono(clientResponse -> clientResponse.bodyToMono(AuthResponse.class))
//                .block();
//        return ResponseEntity.ok(block);

//        assert response != null;
//        return response.getAccess_token();
//        return authResponse.getAccess_token();
    }

    private String encodeValue(String value) {
        return URLEncoder.encode(value, StandardCharsets.UTF_8);
    }

    @GetMapping("message")
    public String message() {
        return "Hello";
    }

}
