package com.jecc.sunat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableFeignClients(basePackages = {"com.jecc.sunat.client"})
@SpringBootApplication
public class SunatApiRestApplication {

    public static void main(String[] args) {
        SpringApplication.run(SunatApiRestApplication.class, args);
    }

}
