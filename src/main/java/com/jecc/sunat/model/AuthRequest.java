package com.jecc.sunat.model;


public class AuthRequest {

    private String grant_type;

    private String client_id;

    private String secret_id;

    private String username;

    private String password;

    private String scope;

    public AuthRequest(String grant_type, String client_id, String secret_id, String username, String password, String scope) {
        this.grant_type = grant_type;
        this.client_id = client_id;
        this.secret_id = secret_id;
        this.username = username;
        this.password = password;
        this.scope = scope;
    }
}
