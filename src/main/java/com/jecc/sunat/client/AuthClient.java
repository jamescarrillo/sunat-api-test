package com.jecc.sunat.client;

import com.jecc.sunat.config.CoreFeignConfiguration;
import com.jecc.sunat.model.AuthResponse;
import feign.Headers;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;


@FeignClient(
        name = "auth-sunat",
        url = "https://api-seguridad.sunat.gob.pe/v1/clientessol/c53c1d64-39ea-45b4-ae9b-82f7cca3d947",
        configuration = CoreFeignConfiguration.class)
public interface AuthClient {

    @PostMapping(consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Headers("Content-Type: application/x-www-form-urlencoded")
    AuthResponse getToken(@RequestParam Map<String, String> authRequest);
//
//    @PostMapping(consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE,
//            produces = MediaType.APPLICATION_JSON_VALUE)
//    @Headers("Content-Type: application/x-www-form-urlencoded")
//    AuthResponse getToken(@Param("grant_type") String grantType, @Param("client_id") String clientId,
//                          @Param("client_secret") String clientSecret, @Param("username") String username,
//                          @Param("password") String password, @Param("scope") String scope);

    @PostMapping(value = "/oauth2/token", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    @Headers("Content-Type: application/x-www-form-urlencoded")
    AuthResponse activate(Map<String, String> formParams);

}
