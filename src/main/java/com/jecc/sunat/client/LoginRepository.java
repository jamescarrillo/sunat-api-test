package com.jecc.sunat.client;

import com.jecc.sunat.config.CoreFeignConfiguration;
import com.jecc.sunat.model.AuthResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;

import static org.springframework.http.MediaType.APPLICATION_FORM_URLENCODED_VALUE;

@FeignClient(
        name = "auth-sunat-second",
        url = "https://api-seguridad.sunat.gob.pe/v1/clientessol/${application.sunat-cliente-sol}",
        configuration = CoreFeignConfiguration.class)
public interface LoginRepository {

    @PostMapping(value = "/oauth2/token", consumes = APPLICATION_FORM_URLENCODED_VALUE)
    AuthResponse signIn(@RequestBody Map<String, ?> form);
}
